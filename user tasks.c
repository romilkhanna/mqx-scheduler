#include "defines.h"

extern _pool_id message_pool;
extern _queue_id sched_q;
extern _queue_id taskr_q;
extern MQX_TICK_STRUCT global_start_time;

/*
 * Purpose: Run task once for specified amount of time.
 * Input: The amount of time to run for.
 * Output: None
 */
void aperiodic_task(int input) 
{	
	// count till input time
	int i;
	
	//printf("Running aperiod task with execution %d\n", input);
	printf("Running aperiodic task.\n");
	for (i = 0; i < input; i++)
	{
		// Don't track how long as passed because execution time is best effort.
		_time_delay_ticks(1);
	}
	
	_task_block();
}

/*
 * Purpose: Run task once for specified amount of time.
 * Input: The amount of time to run for.
 * Output: None
 */
void periodic_task(int execution_time) 
{
	uint_32 mytid;
	MQX_TICK_STRUCT temp;
	
	_time_init_ticks(&temp, 0);	
	_time_set_ticks(&temp);
	
	mytid = _task_get_id();	
		
	//printf("Running periodic task with execution %d\n", execution_time);
	while (temp.TICKS[1] < execution_time)
	{
		// Keep track of time because execution time matters
		_time_get_ticks(&temp);
		_time_delay_ticks(1);
	}
	
	//printf("Deleted self: id: %d, execution: %d\n", mytid, execution_time);	
	
	// tell monitor im done
	mytid = dd_delete(mytid);
	
	if (mytid == 0) 
	{	
		printf("Request to delete failed for task: %d\n", execution_time);
	}
	
	// I finished so kill myself.
	_task_abort(_task_abort(_task_get_id()));
}
