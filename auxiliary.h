#ifndef AUXILIARY_H
#define AUXILIARY_H

#include "defines.h"

/*
 * Purpose: Return the active list.
 * Input: Reference list.
 * Output: Success/Fail
 */
uint_32 dd_return_active_list(task_list *);

/*
 * Purpose: Return the overdue list.
 * Input: Reference list.
 * Output: Success/Fail
 */
uint_32 dd_return_overdue_list(task_list *);

/*
 * Purpose: Create a task struct and a MQX task, requests scheduler to schedule task.
 * Input: Template index and deadline.
 * Output: Task ID of the created task or 0.
 */
_task_id dd_tcreate(uint_32, uint_32);

/*
 * Purpose: Finds and sets the lowest priority parameter.
 * Input: Active task list.
 * Output: None.
 */
void getLowestPriority(task_list *);

/*
 * Purpose: Sets up periodic task 1 with a specified name and deadline then calls dd_tcreate().
 * Input: None.
 * Output: None.
 */
void *ptask1();

/*
 * Purpose: Sets up periodic task 2 with a specified name and deadline then calls dd_tcreate().
 * Input: None.
 * Output: None.
 */
void *ptask2();

/*
 * Purpose: Sets up periodic task 3 with a specified name and deadline then calls dd_tcreate().
 * Input: None.
 * Output: None.
 */
void *ptask3();

/*
 * Purpose: Sets up aperiodic task 1 with a specified name and deadline then calls dd_tcreate().
 * Input: None.
 * Output: None.
 */
void *atask1();

#endif