#include "auxiliary.h"

// Global definitions
extern _pool_id message_pool;
extern _queue_id sched_q;
extern _queue_id taskr_q;
extern MQX_TICK_STRUCT global_start_time;
extern char q;

// Local definitions
int initParam, lowest_priority = RUNNING_PRIORITY_END;
uint_8 initName;

void aux_task() 
{
	_queue_id aux_q;
	task_list active_list, overdue_list;
	_timer_id timer1, timer2, timer3, timer4;
	MQX_TICK_STRUCT interval1, interval2, interval3, interval4;	
		
	// Setup message queue and lists
	aux_q = _msgq_open(MSGQ_FREE_QUEUE, 0);
	dd_return_active_list(&active_list);
	dd_return_overdue_list(&overdue_list);
	
	// Task 1	
	getLowestPriority(&active_list);	
	interval1.TICKS[0] = 0;
	interval1.TICKS[1] = ptask1_period;
	//printf("Generating periodic task 1 every %d ticks\n", interval1.TICKS[1]);

	// Setup timer to generate this task
	timer1 = _timer_start_periodic_every_ticks(ptask1, NULL, TIMER_ELAPSED_TIME_MODE, &interval1);
	
	// Task 2
	getLowestPriority(&active_list);
	interval2.TICKS[0] = 0;
	interval2.TICKS[1] = ptask2_period;
	//printf("Generating periodic task 2 every %d ticks\n", interval2.TICKS[1]);
	
	// Setup timer to generate this task
	timer2 = _timer_start_periodic_every_ticks(ptask2, NULL, TIMER_ELAPSED_TIME_MODE, &interval2);
	
	// Task 3	
	getLowestPriority(&active_list);
	interval3.TICKS[0] = 0;
	interval3.TICKS[1] = ptask3_period;
	//printf("Generating periodic task 3 every %d ticks\n", interval3.TICKS[1]);
	
	// Setup timer to generate this task
	timer3 = _timer_start_periodic_every_ticks(ptask3, NULL, TIMER_ELAPSED_TIME_MODE, &interval3);
	
	
	// Task 4 (aperiodic)
	getLowestPriority(&active_list);
	interval4.TICKS[0] = 0;
	interval4.TICKS[1] = 50;
	
	// Setup timer to generate this task
	timer4 = _timer_start_periodic_every_ticks(atask1, NULL, TIMER_ELAPSED_TIME_MODE, &interval4);
	
	// Go to sleep and let things happen
	_task_block();
}

void *ptask1() 
{
	_task_id tid;		
	initParam = ptask1_runtime; // Execution time
	initName = 0x01; // Name
	tid = dd_tcreate(WORKER_TASK_PERIODIC, ptask1_deadline); // Create the task
	if (tid == 0)
	{
		printf("Failed to create periodic task 1\n");
	}
}

void *ptask2() 
{
	_task_id tid;		
	initParam = ptask2_runtime; // Execution time
	initName = 0x02; // Name
	tid = dd_tcreate(WORKER_TASK_PERIODIC, ptask2_deadline); // Create the task
	if (tid == 0)
	{
		printf("Failed to create periodic task 2\n");
	}
}

void *ptask3() 
{
	_task_id tid;		
	initParam = ptask3_runtime; // Execution time
	initName = 0x04; // Name
	tid = dd_tcreate(WORKER_TASK_PERIODIC, ptask3_deadline); // Create the task
	if (tid == 0)
	{
		printf("Failed to create periodic task 3\n");
	}
}

void *atask1()
{
	_task_id tid;
	if (q == 'a') // Check keyboard input
	{	
		q = '\0'; // Reset input
		initParam = 400; // Execution time
		initName = 0x08; // Name
		tid = dd_tcreate(WORKER_TASK_APERIODIC, 400); // Create the task
		if (tid == 0)
		{
			printf("Failed to create aperiodic task.\n");
		}	
	}
}

void getLowestPriority(task_list *active_list) 
{
	task_list *cur;
	cur = active_list;
	
	// Find the current lowest priority
	while (cur->next_cell)
	{
		cur = cur->next_cell;
		if (cur->priority < lowest_priority)
		{
			lowest_priority = cur->priority;
		}
	}
}

_task_id dd_tcreate(uint_32 template_index, uint_32 deadline) 
{
	_queue_id create_q;
	task_list *task;
	TASK_MESSAGE *message;
	uint_32 tid;
	int error;
		
	// Create message queues and message
	create_q = _msgq_open(MSGQ_FREE_QUEUE, 0);
	message = _msg_alloc(message_pool);
	
	// Allocate a new task
	task = _mem_alloc(sizeof(task_list));
	task->deadline = deadline;		

	// Check type of task
	if (template_index == WORKER_TASK_APERIODIC) 
	{
		task->task_type = APERIODIC;	
	} else if (template_index == WORKER_TASK_PERIODIC) 
	{
		task->task_type = PERIODIC;		
		
	} else 
	{
		// error
		printf("dd_tcreate(): Invalid task type defined.\n");
		_mem_free(task);
		_msg_free(message);
		_msgq_close(create_q);
		return 0;
	}
	
	// Populate task
	task->execution = initParam;
	task->priority = lowest_priority - 1;			
	
	// Create the task in MQX			
	tid = _task_create_blocked(0, template_index, task->execution);    
    if (tid == 0)
    {
    	printf("dd_tcreate(): received tid 0 from _task_create()\n");
    }
    
    // Check error
    error = _task_get_error();
    if (error == MQX_INVALID_PROCESSOR_NUMBER) 
    {
    	printf("dd_tcreate(): MQX_INVALID_PROCESSOR_NUMBER\n");
    	return;
    } else if (error == MQX_NO_TASK_TEMPLATE) 
    {
    	printf("dd_tcreate(): MQX_NO_TASK_TEMPLATE\n");
    	return;
    } else if (error == MQX_OUT_OF_MEMORY) 
    {
    	printf("dd_tcreate(): MQX_OUT_OF_MEMORY\n");
    	return;
    }
    
    // Task is valid, continue filling it out
    task->tid = tid;
    task->name = initName;
	
	// Prepare message
	message->HEADER.TARGET_QID = sched_q;
	message->HEADER.SOURCE_QID = create_q;
	message->HEADER.SIZE = sizeof(TASK_MESSAGE);
	message->task = task;
	message->request = ADD;
	message->template_index = template_index;
	
	// Send to scheduler
	if (_msgq_send(message) == FALSE) 
	{
		printf("dd_tcreate(): Failed to send message\n");
		_task_destroy(tid);
		_mem_free(task);
		_msg_free(message);
		_msgq_close(create_q);
		return 0;
	}
	
	// Wait for a reply
	message = _msgq_receive(create_q, 0);
	
	// Check if still valid
	if (message->task->tid == 0) 
	{
		// error
		printf("dd_tcreate(): Failed to create task\n");
		_task_destroy(tid);
		_mem_free(task);
		_msg_free(message);
		_msgq_close(create_q);
		return 0;
	}			
	
	_msg_free(message);
	_msgq_close(create_q);
    
    return tid;
}

uint_32 dd_delete(uint_32 task_id) 
{
	_queue_id delete_q;	
	task_list *task;
	TASK_MESSAGE *message;
	
	// Allocate a task
	task = _mem_alloc(sizeof(task_list));
	task->tid = task_id;
	
	// Setup queues and message
	delete_q = _msgq_open(MSGQ_FREE_QUEUE, 0);
	message = _msg_alloc(message_pool);
	message->HEADER.TARGET_QID = taskr_q;
	message->HEADER.SOURCE_QID = delete_q;
	message->HEADER.SIZE = sizeof(TASK_MESSAGE);
	message->task = task;
	message->request = REMOVE;
	
	// Send to scheduler
	if (_msgq_send(message) == FALSE) 
	{
		printf("dd_delete(): Failed to send message\n");
		_mem_free(task);
		_msg_free(message);
		_msgq_close(delete_q);
		return 0;
	}
	
	// Wait for reply
	message = _msgq_receive(delete_q, 0);
	
	// Check if still valid
	if (message->task->tid != task_id) 
	{
		// error
		printf("dd_delete(): Failed to delete task\n");
		_mem_free(task);
		_msg_free(message);
		_msgq_close(delete_q);
		return 0;
	}
	
	_mem_free(task);
	_msg_free(message);
	_msgq_close(delete_q);
    
    return task_id;
}

uint_32 dd_return_active_list(task_list *list) 
{
	_queue_id active_q;
	TASK_MESSAGE *message;
	
	//printf("Retrieving active list\n");
	
	active_q = _msgq_open(MSGQ_FREE_QUEUE, 0);
	message = _msg_alloc(message_pool);
	message->HEADER.TARGET_QID = sched_q;
	message->HEADER.SOURCE_QID = active_q;
	message->HEADER.SIZE = sizeof(TASK_MESSAGE);
	message->request = GET_ACTIVE;
	message->task = list;
	
	if (_msgq_send(message) == FALSE) 
	{
		printf("dd_return_active_list(): Failed to send message\n");
		_msg_free(message);
		_msgq_close(active_q);
		return FAILED;
	}
	
	message = _msgq_receive(active_q, 0);
	
	*list = *(message->task);
	
	_msg_free(message);
	_msgq_close(active_q);
	
	return SUCCESS;
}

uint_32 dd_return_overdue_list(task_list *list) 
{
	_queue_id overdue_q;
	TASK_MESSAGE *message;	
	
	//printf("Retrieving overdue list\n");
	
	overdue_q = _msgq_open(MSGQ_FREE_QUEUE, 0);
	message = _msg_alloc(message_pool);
	message->HEADER.TARGET_QID = sched_q;
	message->HEADER.SOURCE_QID = overdue_q;
	message->HEADER.SIZE = sizeof(TASK_MESSAGE);
	message->request = GET_OVERDUE;
	message->task = list;
	
	if (_msgq_send(message) == FALSE) 
	{
		printf("dd_return_overdue_list(): Failed to send message\n");
		_msg_free(message);
		_msgq_close(overdue_q);
		return FAILED;
	}
	
	message = _msgq_receive(overdue_q, 0);
	
	*list = *(message->task);
	
	_msg_free(message);
	_msgq_close(overdue_q);
	
	return SUCCESS;
}