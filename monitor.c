#include "defines.h"

extern MQX_TICK_STRUCT global_start_time, idle_count;
extern MONITOR_INFO monitor_info;

uint_32 idle_time = 0;

void *monitor_task()
{
	uint_32 time_now;
	uint_32 utilization = 0, overhead = 0, idle_pct;

	_time_get_ticks(&idle_count);
	idle_time += idle_count.TICKS[1];
	_time_get_elapsed_ticks(&global_start_time);
	time_now = global_start_time.TICKS[1];
	
	if (time_now == 0 || monitor_info.num_run1 == 0 || monitor_info.num_run2 == 0 
			|| monitor_info.num_run3 == 0)
	{
		return;
	}

	// do aperiodic first
	utilization += (monitor_info.exec_time_aperiodic1 * 100) / time_now;
	
	// periodic 1
	utilization += (monitor_info.exec_time1 * 100) / time_now;
	
	// periodic 2
	utilization += (monitor_info.exec_time2 * 100) / time_now;	
	
	// periodic 3
	utilization += (monitor_info.exec_time3 * 100) / time_now;
	
	// overhead
	idle_pct = (idle_time * 100) / time_now;
	overhead = 100 - utilization;
	overhead -= idle_pct;	
	
	_time_get_elapsed_ticks(&global_start_time);
	time_now = global_start_time.TICKS[1];
	
	printf("(%d) Total utilization: %d%%, System Overhead: %d%%\n", time_now, utilization, overhead);
}