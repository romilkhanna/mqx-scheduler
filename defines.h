#ifndef DEFINES_H
#define DEFINES_H

#include <mqx.h>
#include <message.h>
#include <bsp.h>
#include <fio.h>
#include <timer.h>
#include "MCF52259.h"

// Queue IDs
#define SCHED_QID 8
#define TASKR_QID 9

/* Task IDs */
#define SCHEDULER_TASK 5
#define AUXILIARY_TASK 6
#define WORKER_TASK_APERIODIC 7
#define WORKER_TASK_PERIODIC 8
#define IDLE_COUNTER 9

#define INIT_ADD_REQ 3

// Obvious
#define MAX_TASKS 10

/* Priorities */
#define SCHEDULER_PRIORITY 2
#define AUXILIARY_PRIORITY 4
#define RUNNING_PRIORITY_START 3
#define RUNNING_PRIORITY_END (RUNNING_PRIORITY_START + MAX_TASKS)

#define ptask1_runtime 100
#define ptask1_period 300
#define ptask1_deadline 300

#define ptask2_runtime 150
#define ptask2_period 400
#define ptask2_deadline 400

#define ptask3_runtime 50
#define ptask3_period 500
#define ptask3_deadline 500

// Define all requests
enum task_request 
{
	ADD, REMOVE, GET_ACTIVE, GET_OVERDUE, DONE
};

// Define type of tasks
enum _task_type
{
	PERIODIC, APERIODIC
};

// Define status types
enum _status_type 
{
	SUCCESS, FAILED
};

// Define task structure
typedef struct task_list
{
	uint_32 tid;
	uint_32 deadline;
	uint_32 task_type;
	uint_32 creation_time;
	uint_32 priority;
	uint_32 execution;
	uint_8 name;
	struct task_list *next_cell;
	struct task_list *previous_cell;
} task_list, *task_list_ptr;

// Define message
typedef struct TASK_MESSAGE
{
	MESSAGE_HEADER_STRUCT HEADER; // This is where it's going/coming from
	task_list *task;
	uint_32 template_index;
	enum task_request request;
} TASK_MESSAGE, *TASK_MESSAGE_PTR;

// Define information for monitoring
typedef struct MONITOR_INFO
{
	uint_32 exec_time1;
	uint_32 num_run1;
	uint_32 exec_time2;
	uint_32 num_run2;
	uint_32 exec_time3;
	uint_32 num_run3;
	uint_32 exec_time_aperiodic1;
	uint_32 num_runs_aperiodic1;
} MONITOR_INFO;

/*
 * Purpose: Addds the given task to the active list.
 * Input: Task, active list
 * Output: Task ID if success, 0 otherwise
 */
uint_32 addTask(task_list *, task_list *);

/*
 * Purpose: Deletes the given task from the active list.
 * Input: Task ID, active list
 * Output: Task ID if success, 0 otherwise
 */
uint_32 deleteActive(uint_32 tid, task_list *);

/*
 * Purpose: Deletes the task from the overdue list.
 * Input: Task ID, overdue list
 * Output: Task ID if success, 0 otherwise
 */
uint_32 deleteOverdue(uint_32 tid, task_list *);

/*
 * Purpose: Schedules tasks in the active list, enforces deadlines and moves tasks to overdue.
 * Input: None
 * Output: None
 */
void dd_scheduler_task(uint_32);

/*
 * Purpose: Runs the auxiliary task.
 * Input: None
 * Output: None
 */
void aux_task();

/*
 * Purpose: Runs the aperiodic task.
 * Input: Execution time
 * Output: None
 */
void aperiodic_task(int);

/*
 * Purpose: Runs the periodic task.
 * Input: Execution time
 * Output: None
 */
void periodic_task(int);

/*
 * Purpose: Sets all tasks in the active list to ready.
 * Input: Active list
 * Output: None
 */
void readyTasks(task_list *);

/*
 * Purpose: Moves task from active list to overdue list.
 * Input: Task, Active list, Overdue list
 * Output: None
 */
void updateOverdue(task_list *, task_list *, task_list *);

/*
 * Purpose: Prints the contents of a given list.
 * Input: List
 * Output: Contents of list
 */
void printList(task_list *);

/*
 * Purpose: Adds a task to the overdue list.
 * Input: Task, Overdue list
 * Output: None
 */
void addOverdue(task_list *, task_list *);

/*
 * Purpose: Initializes LEDs to use.
 * Input: None
 * Output: None
 */
void init_led();

/*
 * Purpose: Runs the monitor task to print usage data periodically.
 * Input: None
 * Output: None
 */
void *monitor_task();

/*
 * Purpose: Counts the amount of time spent idle.
 * Input: None
 * Output: None
 */
void idle_counter();

/*
 * Purpose: Sets up the ISR for keyboard input.
 * Input: None
 * Output: None
 */
void setupISR();

/*
 * Purpose: Initializes the input ISR.
 * Input: None
 * Output: None
 */
void uartISR();

/*
 	This function takes the id of the task to remove from the list.
 	It searches the active list and the overdue list then notifies the scheduler
 	to remove the requested task.
 	Upon success it returns the removed task's id otherwise error.
 */
uint_32 dd_delete(uint_32);



#endif