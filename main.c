/**HEADER********************************************************************
* 
* Copyright (c) 2008 Freescale Semiconductor;
* All Rights Reserved
*
*************************************************************************** 
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************
*
* $FileName: main.c$
* $Version : 3.0.2.0$
* $Date    : Nov-21-2008$
*
* Comments:
*
*   This file contains the source for your new program.
*
*END************************************************************************/

#include "defines.h"

TASK_TEMPLATE_STRUCT  MQX_template_list[] = 
{ 
    {SCHEDULER_TASK, dd_scheduler_task, 1500, SCHEDULER_PRIORITY, "sched", MQX_AUTO_START_TASK, 0, 0},
    {AUXILIARY_TASK, aux_task, 1500, AUXILIARY_PRIORITY, "aux", MQX_AUTO_START_TASK, 0, 0},
    {WORKER_TASK_APERIODIC, aperiodic_task, 1500, RUNNING_PRIORITY_START, "aworker", 0, 0, 0},
    {WORKER_TASK_PERIODIC, periodic_task, 1500, RUNNING_PRIORITY_START, "worker", 0, 0, 0},
    {IDLE_COUNTER, idle_counter, 1500, RUNNING_PRIORITY_END, "idler", MQX_AUTO_START_TASK, 0, 0},
    {0,          0,          0,   0, 0,       0,                   0, 0}
};

/*TASK*-----------------------------------------------------
* 
* Task Name    : dd_scheduler_task
* Comments     :
*    This task...
*
*END*-----------------------------------------------------*/

// Globals
_pool_id message_pool;
_queue_id sched_q;
_queue_id taskr_q;
uint_32 uartRegisterMask = 0; 
char q = '\0';
MQX_TICK_STRUCT global_start_time, idle_count;
MONITOR_INFO monitor_info;


//uartISR contains the interrupt service routine for the UART. 
//This code will get you started but will not be enough for the project. 
void uartISR ()
{
	_int_disable();
	//Check if the UART is ready to receive a character
	if(MCF_UART0_USR & MCF_UART_USR_RXRDY)
	{
		q = MCF_UART0_URB; //Store character in RX buffer to q. 
	}
	_int_enable();
}//end uartISR


//Set up the interrupt service routine for the UART
//Set the UART to generate an interrupt whenever a character is received from the keyboard.
void setupISR ()
{
	uartRegisterMask = MCF_UART_UIMR_FFULL_RXRDY; //Unmask Fifo Full RX interrupt 
	MCF_UART0_UMR2 |= MCF_UART_UMR_CM_NORMAL;	//Set loopback mode to normal
	MCF_UART0_UIMR = uartRegisterMask; //Write value to register
	//install interrupt vector
	_int_install_isr(64+13, &uartISR, NULL ); 
	
	//unmask global interrupts
	MCF_INTC0_IMRL &= ~( MCF_INTC_IMRL_MASKALL | MCF_INTC_IMRL_INT_MASK13);
	
	//set priority and level
	MCF_INTC0_ICR13 |= MCF_INTC_ICR_IP(0x5) | MCF_INTC_ICR_IL(0x5);		
}//end setupISR


void dd_scheduler_task(uint_32 initial_data)
{   		
	TASK_MESSAGE *message;
	task_list active_list, overdue_list;
	uint_32 deadline, creationTime, currentTime;
	_timer_id timerM;
	MQX_TICK_STRUCT monitor_interval;	
	int error;	

	init_led();
	setupISR();
	monitor_interval.TICKS[0] = 0;
	monitor_interval.TICKS[1] = 2000;

	// Setup monitor timer
	timerM = _timer_start_periodic_every_ticks(monitor_task, NULL, TIMER_ELAPSED_TIME_MODE, &monitor_interval);
	
	active_list.next_cell = NULL;
	overdue_list.next_cell = NULL;
	
	/* Create msgpool */
	message_pool = _msgpool_create(sizeof(TASK_MESSAGE), INIT_ADD_REQ, INIT_ADD_REQ, 0);
	sched_q = _msgq_open(SCHED_QID, 0);
	taskr_q = _msgq_open(TASKR_QID, 0);
	
	while (1) 
	{		
		_time_get_elapsed_ticks(&global_start_time);
		// get message from aux
		message = _msgq_receive_ticks(sched_q, 5);
		
		if (message == NULL)
		{
			if (active_list.next_cell != NULL) 
			{
				// Update global time
				_time_get_elapsed_ticks(&global_start_time);
				currentTime = global_start_time.TICKS[1];
								
				deadline = active_list.next_cell->deadline;
				creationTime = active_list.next_cell->creation_time;
				
				// Ready first task and check errors
				_task_ready(_task_get_td(active_list.next_cell->tid));				
				error = _task_get_error();
			    if (error == MQX_INVALID_TASK_ID) 
			    {
			    	printf("readyTasks(): MQX_INVALID_TASK_ID\n");
			    	continue;
			    } else if (error == MQX_INVALID_TASK_STATE) 
			    {
			    	printf("readyTasks(): MQX_INVALID_TASK_STATE\n");
			    	continue;
			    }
				//readyTasks(&active_list);
				
				//Enable Lights
				MCF_GPIO_SETTC |= 0x000000F & active_list.next_cell->name;
				
				// wait to hear from the task				
				message = _msgq_receive_ticks(taskr_q, deadline - currentTime);	
				
				//Disable Lights
				MCF_GPIO_CLRTC &= 0xFF;
				
				// Update monitor information based on task name
				switch(active_list.next_cell->name)
				{
					case 0x01:
						if(message != NULL)
						{
							monitor_info.exec_time1 += active_list.next_cell->execution;		
						} else 
						{
							monitor_info.exec_time1 += deadline - currentTime;
						}
						monitor_info.num_run1++;
						break;
					case 0x02:
						if(message != NULL)
						{
							monitor_info.exec_time2 += active_list.next_cell->execution;		
						} else 
						{
							monitor_info.exec_time2 += deadline - currentTime;
						}
						monitor_info.num_run2++;
						break;												
					case 0x04:
						if(message != NULL)
						{
							monitor_info.exec_time3 += active_list.next_cell->execution;		
						} else 
						{
							monitor_info.exec_time3 += deadline - currentTime;
						}
						monitor_info.num_run3++;
						break;
					case 0x08:
						monitor_info.exec_time_aperiodic1 += active_list.next_cell->execution;						
						monitor_info.num_runs_aperiodic1++;
						break;						
				}
								
				
				if (message == NULL && active_list.next_cell->task_type == PERIODIC) 
				{
					//overdue
					//printf("Reached deadline at %d.\n", deadline);
				    updateOverdue(active_list.next_cell, &overdue_list, &active_list);
				}
				else if (message->request == REMOVE || active_list.next_cell->task_type == APERIODIC) 
				{
					deleteActive(active_list.next_cell->tid, &active_list);
				}
			} else 
			{
				continue;
			}			
		} else 
		{
			// New request from aux, find out what it was and do it.
			message->HEADER.TARGET_QID = message->HEADER.SOURCE_QID;
			message->HEADER.SOURCE_QID = sched_q;
			
			switch (message->request) 
			{
				case ADD:	
					_time_get_elapsed_ticks(&global_start_time);
					currentTime = global_start_time.TICKS[1];
					if (currentTime >= message->task->deadline + message->task->creation_time)
					{						
						addOverdue(message->task, &overdue_list);
						break;
					}
					message->task->tid = addTask(message->task, &active_list);					
					//printf("(%d) Added task with tid: %d, execution: %d, deadline: %d\n", currentTime, message->task->tid, message->task->execution, message->task->deadline);
					break;
				case REMOVE:
					//printf("Deleting task %d\n", message->task->tid);
					message->task->tid = deleteActive(message->task->tid, &active_list);
					if (message->task->tid == 0)
					{
						message->task->tid = deleteOverdue(message->task->tid, &overdue_list);
					}
					break;
				case GET_ACTIVE:
					//printf("Getting active list\n");
					message->task = &active_list;
					break;
				case GET_OVERDUE:
					//printf("Getting overdue list\n");
					message->task = &overdue_list;
					break;
			}
			
			_msgq_send(message);	
		}				
	}

	_mqx_exit(0);
}

void idle_counter()
{
	_time_init_ticks(&idle_count, 0);
	_time_set_ticks(&idle_count);
	while(1)
	{
		// Waste time
		_time_get_ticks(&idle_count);
		_time_delay_ticks(1);		
	}
}

void addOverdue(task_list *task, task_list *overdue_list)
{
	task_list *cur;
	cur = overdue_list;
	while(cur->next_cell) 
	{
		cur = cur->next_cell;
	}
	cur->next_cell = task;
	task->previous_cell = cur;
	task->next_cell = NULL;
	
	_task_destroy(task->tid);
	
	MCF_GPIO_SETTC |= 0x000000F;
	
	//printf("Moved task %d to overdue\n", task->execution);
	
	//Disable Lights
	MCF_GPIO_CLRTC &= 0xFF;
}

void updateOverdue(task_list *task, task_list *overdue_list, task_list *active_list) 
{
	task_list *cur;
	cur = overdue_list;
	while(cur->next_cell) 
	{
		cur = cur->next_cell;
	}
	
	task->next_cell->previous_cell = active_list;
	active_list->next_cell = task->next_cell;
	
	cur->next_cell = task;
	task->previous_cell = cur;
	task->next_cell = NULL;
	
	//printf("List after add overdue: ");
	//printList(overdue_list);
	
	_task_destroy(task->tid);
}

void printList(task_list *list)
{
	task_list *cur;
	cur = list;
	while(cur->next_cell) 
	{
		cur = cur->next_cell;
		printf("%d ", cur->execution);
	}
	printf("\n");
}

void readyTasks(task_list *active_list) 
{
	task_list *cur;
	int error;
	cur = active_list;
	while(cur->next_cell) 
	{
		cur = cur->next_cell;
		_task_ready(_task_get_td(cur->tid));
		error = _task_get_error();
	    if (error == MQX_INVALID_TASK_ID) 
	    {
	    	printf("readyTasks(): MQX_INVALID_TASK_ID\n");
	    	break;
	    } else if (error == MQX_INVALID_TASK_STATE) 
	    {
	    	printf("readyTasks(): MQX_INVALID_TASK_STATE\n");
	    	break;
	    }
	}
}

uint_32 addTask(task_list *newTask, task_list *active_list)
{	
	task_list *cur;
	cur = active_list;	
	
	_time_get_elapsed_ticks(&global_start_time);
	newTask->creation_time = global_start_time.TICKS[1];
	newTask->deadline = newTask->creation_time + newTask->deadline;
	
	while(cur->next_cell) 
	{
		cur = cur->next_cell;
					
		if (newTask->deadline < cur->deadline)
		{
			newTask->previous_cell = cur->previous_cell;
			newTask->next_cell = cur;
			cur->previous_cell->next_cell = newTask;			
			cur->previous_cell = newTask;
			
			//printf("List after add: ");
			//printList(active_list);
			
			return newTask->tid;
		}
	}
	
	cur->next_cell = newTask;
	newTask->previous_cell = cur;	
	newTask->next_cell = NULL;
	
	//printf("List after add: ");
	//printList(active_list);
	
	//printf("(%d) [Created task] exec: %d, deadline: %d\n", newTask->creation_time, newTask->execution, newTask->deadline);
	
	return newTask->tid;
}

uint_32 deleteActive(uint_32 tid, task_list *active_list)
{
	task_list *cur;
	// remove from active list
	cur = active_list;
	while(cur->next_cell) 
	{
		cur = cur->next_cell;
		if (cur->tid == tid)
		{
			//printf("deleteTask(active): tid: %d, execution: %d, deadline: %d\n", cur->tid, cur->execution, cur->deadline);
			cur->previous_cell->next_cell = cur->next_cell;
			if (cur->next_cell) 
			{
				cur->next_cell->previous_cell = cur->previous_cell;
			}
			_mem_free(cur);
			if (_task_destroy(tid) == MQX_INVALID_TASK_ID)
			{
				return 0;
			}
			
			//printf("List after remove from active: ");
			//printList(active_list);
			
			return tid;
		}
	}	
	return 0;
}

uint_32 deleteOverdue(uint_32 tid, task_list *overdue_list)
{
	task_list *cur;
	// remove from overdue list
	cur = overdue_list;
	while(cur->next_cell) 
	{
		cur = cur->next_cell;
		if (cur->tid == tid)
		{
			//printf("deleteTask(active): tid: %d, execution: %d, deadline: %d\n", cur->tid, cur->execution, cur->deadline);
			cur->previous_cell->next_cell = cur->next_cell;
			cur->next_cell->previous_cell = cur->previous_cell;			
			if (cur->next_cell) 
			{
				cur->next_cell->previous_cell = cur->previous_cell;
			}
			_mem_free(cur);
			if (_task_destroy(tid) == MQX_INVALID_TASK_ID)
			{
				return 0;
			}
			
			//printf("List after remove from overdue: ");
			//printList(overdue_list);
			
			return tid;
		}
	}		
	
	return 0;
}

void init_led()
{
	MCF_GPIO_PTCPAR &= 0xF0;
	MCF_GPIO_DDRTC |= 0x0F;
	MCF_GPIO_CLRTC &= 0xF0;
}

/* EOF */
